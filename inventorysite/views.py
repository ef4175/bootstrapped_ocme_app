from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from .form import EmployeeForm, LoginForm, SearchForm, ComputerForm, WirelessDeviceForm, IPPhoneForm, LaptopForm, PrinterForm, BarcodeScannerForm, BarcodeScannerCradleForm, ScannerForm, BiometricScannerForm
from .models import Employee, Computer, WirelessDevice, IPPhone, Laptop, Printer, BarcodeScanner, BarcodeScannerCradle, Scanner, BiometricScanner 
from django.forms.formsets import formset_factory, BaseFormSet
from django.forms.models import inlineformset_factory, modelformset_factory
from django.views.generic import CreateView
import datetime

def home_page(request):
	if request.method == "POST": #User attempts login
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username = username, password = password)
		if user:
			login(request, user)
			return HttpResponseRedirect('/')
		else:
			form = LoginForm()
			return render(request, 'inventorysite/home_page.html', {'form': form, 'invalid_login': True})
	else:
		form = LoginForm()
	return render(request, 'inventorysite/home_page.html', {'form': form})

def search_page(request):
	if request.method == "POST":
		form = SearchForm(request.POST)
		if form.is_valid():
			search = form.save(commit = False)
			return redirect('inventorysite.views.search_results', s = search.search_term)
	else:
		form = SearchForm()
	return render(request, 'inventorysite/search_page.html', {'form': form}) 

#Discard forms if they are completely empty and check if newly-added employee's device(s) has a match with a device belonging to OCME
@login_required
def add_page(request):
	root_employee = get_object_or_404(Employee, pk = 1)
	ComputerFormset = formset_factory(ComputerForm, extra = 1)
	WDFormset = formset_factory(WirelessDeviceForm, extra = 1)
	PhoneFormset = formset_factory(IPPhoneForm, extra = 1)
	LaptopFormset = formset_factory(LaptopForm, extra = 1)
	PrinterFormset = formset_factory(PrinterForm, extra = 1)
	BarcodeScannerFormset = formset_factory(BarcodeScannerForm, extra = 1)
	CradleFormset = formset_factory(BarcodeScannerCradleForm, extra = 1)
	ScannerFormset = formset_factory(ScannerForm, extra = 1)
	BioFormset = formset_factory(BiometricScannerForm, extra = 1)
	employee_form = EmployeeForm()
	if request.method == "POST":
		computer_formset = ComputerFormset(request.POST, prefix = "computers")
		wd_formset = WDFormset(request.POST, prefix = "wdevices")
		phone_formset = PhoneFormset(request.POST, prefix = "phones")
		laptop_formset = LaptopFormset(request.POST, prefix = "laptops")
		printer_formset = PrinterFormset(request.POST, prefix = "printers")
		barcode_formset = BarcodeScannerFormset(request.POST, prefix = "barscanners")
		cradle_formset = CradleFormset(request.POST, prefix = "cradles")
		scanner_formset = ScannerFormset(request.POST, prefix = "scanners")
		bio_formset = BioFormset(request.POST, prefix = "bioscanners")
		employee = EmployeeForm(request.POST)
		if employee.is_valid() and computer_formset.is_valid() and wd_formset.is_valid() and phone_formset.is_valid() and laptop_formset.is_valid() and printer_formset.is_valid() and barcode_formset.is_valid() and cradle_formset.is_valid() and scanner_formset.is_valid() and bio_formset.is_valid():
			emp = employee.save(commit = False)
			emp.save()
			for computer in computer_formset:
				c = computer.save(commit = False)
				c.pc_user = emp
				c.save()
				if c.pc_manufacturer == "" and c.pc_model == "" and c.pc_host_name == "" and c.pc_service_tag == "" and c.pc_asset_tag == "" and c.pc_owner == "":
					c.delete()
				for root_computer in root_employee.computers.all():
					if c.pc_host_name == root_computer.pc_host_name and c.pc_service_tag == root_computer.pc_service_tag and c.pc_asset_tag == root_computer.pc_asset_tag:
						root_computer.delete()
			for wd in wd_formset:
				w = wd.save(commit = False)
				w.wd_user = emp
				w.save()
				if w.wd_type == "" and w.wd_manufacturer == "" and w.wd_carrier == "" and w.wd_model == "" and w.wd_number == "" and w.wd_serial_number == "" and w.wd_meid == "" and w.wd_asset_tag == "":
					w.delete()
				for root_wd in root_employee.wdevices.all():
					if w.wd_serial_number == root_wd.wd_serial_number and w.wd_asset_tag == root_wd.wd_asset_tag:
						root_wd.delete()
			for phone in phone_formset:
				p = phone.save(commit = False)
				p.phone_user = emp
				p.save()
				if p.phone_manufacturer == "" and p.phone_model == "" and p.phone_number == "" and p.phone_serial_number == "" and p.phone_mac == "" and p.phone_asset_tag == "":
					p.delete()
				for root_phone in root_employee.phones.all():
					if p.phone_serial_number == root_phone.phone_serial_number and p.phone_asset_tag == root_phone.phone_asset_tag:
						root_phone.delete()
			for laptop in laptop_formset:
				l = laptop.save(commit = False)
				l.laptop_user = emp
				l.save()
				if l.laptop_manufacturer == "" and l.laptop_model == "" and l.laptop_host_name == "" and l.laptop_service_tag == "" and l.laptop_asset_tag == "" and l.laptop_owner == "":
					l.delete()
				for root_laptop in root_employee.laptops.all():
					if l.laptop_host_name == root_laptop.laptop_host_name and l.laptop_service_tag == root_laptop.laptop_service_tag and l.laptop_asset_tag == root_laptop.laptop_asset_tag:
						root_laptop.delete()
			for printer in printer_formset:
				p = printer.save(commit = False)
				p.printer_user = emp
				p.save()
				if p.printer_manufacturer == "" and p.printer_model == "" and p.printer_serial_number == "" and p.printer_asset_tag == "" and p.printer_owner == "":
					p.delete()
				for root_printer in root_employee.printers.all():
					if p.printer_serial_number == root_printer.printer_serial_number and p.printer_asset_tag == root_printer.printer_asset_tag:
						root_printer.delete()
			for barscanner in barcode_formset:
				b = barscanner.save(commit = False)
				b.bs_user = emp
				b.save()
				if b.bs_manufacturer == "" and b.bs_model == "" and b.bs_serial_number == "" and b.bs_asset_tag == "":
					b.delete()
				for root_barscanner in root_employee.bscanners.all():
					if b.bs_serial_number == root_barscanner.bs_serial_number and b.bs_asset_tag == root_barscanner.bs_asset_tag:
						root_barscanner.delete()
			for cradle in cradle_formset:
				c = cradle.save(commit = False)
				c.bsc_user = emp
				c.save()
				if c.bsc_manufacturer == "" and c.bsc_model == "" and c.bsc_serial_number == "" and c.bsc_asset_tag == "":
					c.delete()
				for root_cradle in root_employee.cradles.all():
					if c.bsc_serial_number == root_cradle.bsc_serial_number and c.bsc_asset_tag == root_cradle.bsc_asset_tag:
						root_cradle.delete()
			for scanner in scanner_formset:
				s = scanner.save(commit = False)
				s.scanner_user = emp
				s.save()
				if s.scanner_manufacturer == "" and s.scanner_model == "" and s.scanner_serial_number == "" and s.scanner_asset_tag == "":
					s.delete()
				for root_scanner in root_employee.scanners.all():
					if s.scanner_serial_number == root_scanner.scanner_serial_number and s.scanner_asset_tag == root_scanner.scanner_asset_tag:
						root_scanner.delete()
			for bioscanner in bio_formset:
				b = bioscanner.save(commit = False)
				b.bis_user = emp
				b.save()
				if b.bis_manufacturer == "" and b.bis_model == "" and b.bis_serial_number == "" and b.bis_asset_tag == "":
					b.delete()
				for root_bioscanner in root_employee.bioscanners.all():
					if b.bis_serial_number == root_bioscanner.bis_serial_number and b.bis_asset_tag == root_bioscanner.bis_asset_tag:
						root_bioscanner.delete()
		return redirect('inventorysite.views.show_employee_info', pk = emp.pk)
	else:
		employee_form = EmployeeForm()
		computer_formset = ComputerFormset(prefix = "computers")
		wd_formset = WDFormset(prefix = "wdevices")
		phone_formset = PhoneFormset(prefix = "phones")
		laptop_formset = LaptopFormset(prefix = "laptops")
		printer_formset = PrinterFormset(prefix = "printers")
		barcode_formset = BarcodeScannerFormset(prefix = "barscanners")
		cradle_formset = CradleFormset(prefix = "cradles")
		scanner_formset = ScannerFormset(prefix = "scanners")
		bio_formset = BioFormset(prefix = "bioscanners")
		return render(request, 'inventorysite/add_page.html', {'form': employee_form, 'computer_formset': computer_formset, 'wd_formset': wd_formset, 'phone_formset': phone_formset, 'laptop_formset': laptop_formset, 'printer_formset': printer_formset, 'barcode_formset': barcode_formset, 'cradle_formset': cradle_formset, 'scanner_formset': scanner_formset, 'bio_formset': bio_formset,})

@login_required
def logout_page(request):
	logout(request)
	return HttpResponseRedirect('/')

#Avert case sensitivity and check if search term is a substring of anything in database	
def search_results(request, s):
	all_employees = Employee.objects.all()
	all_computers = Computer.objects.all()
	all_wdevices = WirelessDevice.objects.all()
	all_phones = IPPhone.objects.all()
	all_laptops = Laptop.objects.all()
	all_printers = Printer.objects.all()
	all_barscanners = BarcodeScanner.objects.all()
	all_cradles = BarcodeScannerCradle.objects.all()
	all_scanners = Scanner.objects.all()
	all_bioscanners = BiometricScanner.objects.all()
	match_employees = []
	match_computers = []
	match_wdevices = []
	match_phones = []
	match_laptops = []
	match_printers = []
	match_barscanners = []
	match_cradles = []
	match_scanners = []
	match_bioscanners = []
	for employee in all_employees:
		if (s.lower() in employee.first_name.lower() or s.lower() in employee.last_name.lower() or s.lower() in employee.location.lower() or s.lower() in employee.department.lower() or s.lower() in employee.employee_id.lower() or s.lower() in employee.email.lower()):
			match_employees.append(employee)
	for computer in all_computers:
		if(s.lower() in computer.pc_manufacturer.lower() or s.lower() in computer.pc_model.lower() or s.lower() in computer.pc_host_name.lower() or s.lower() in computer.pc_service_tag.lower() or s.lower() in computer.pc_asset_tag.lower() or s.lower() in computer.pc_owner.lower()):
			match_computers.append(computer)
	for wd in all_wdevices:
		if(s.lower() in wd.wd_type.lower() or s.lower() in wd.wd_manufacturer.lower() or s.lower() in wd.wd_carrier.lower() or s.lower() in wd.wd_model.lower() or s.lower() in wd.wd_number.lower() or s.lower() in wd.wd_serial_number.lower() or s.lower() in wd.wd_meid or s.lower() in wd.wd_asset_tag.lower() or s.lower() in str(wd.wd_date_deployed).lower()):
			match_wdevices.append(wd)
	for phone in all_phones:
		if(s.lower() in phone.phone_manufacturer.lower() or s.lower() in phone.phone_model.lower() or s.lower() in phone.phone_number.lower() or s.lower() in phone.phone_serial_number.lower() or s.lower() in phone.phone_mac.lower() or s.lower() in phone.phone_asset_tag.lower()):
			match_phones.append(phone)
	for laptop in all_laptops:
		if(s.lower() in laptop.laptop_manufacturer.lower() or s.lower() in laptop.laptop_model.lower() or s.lower() in laptop.laptop_host_name.lower() or s.lower() in laptop.laptop_service_tag.lower() or s.lower() in laptop.laptop_asset_tag.lower() or s.lower() in laptop.laptop_owner.lower()):
			match_laptops.append(laptop)
	for printer in all_printers:
		if(s.lower() in printer.printer_manufacturer.lower() or s.lower() in printer.printer_model.lower() or s.lower() in printer.printer_serial_number.lower() or s.lower() in printer.printer_asset_tag.lower() or s.lower() in printer.printer_owner.lower()):
			match_printers.append(printer)
	for bs in all_barscanners:
		if(s.lower() in bs.bs_manufacturer.lower() or s.lower() in bs.bs_model.lower() or s.lower() in bs.bs_serial_number.lower() or s.lower() in bs.bs_asset_tag.lower()):
			match_barscanners.append(bs)
	for cradle in all_cradles:
		if(s.lower() in cradle.bsc_manufacturer.lower() or s.lower() in cradle.bsc_model.lower() or s.lower() in cradle.bsc_serial_number.lower() or s.lower() in cradle.bsc_asset_tag.lower()):
			match_cradles.append(cradle)
	for scanner in all_scanners:
		if(s.lower() in scanner.scanner_manufacturer.lower() or s.lower() in scanner.scanner_model.lower() or s.lower() in scanner.scanner_serial_number.lower() or s.lower() in scanner.scanner_asset_tag.lower()):
			match_scanners.append(scanner)
	for bio in all_bioscanners:
		if(s.lower() in bio.bis_manufacturer.lower() or s.lower() in bio.bis_model.lower() or s.lower() in bio.bis_serial_number.lower() or s.lower() in bio.bis_asset_tag.lower()):
			match_bioscanners.append(bio)
	return render(request, 'inventorysite/search_results_page.html', {'match_employees': match_employees, 'match_computers': match_computers, 'match_wdevices': match_wdevices, 'match_phones': match_phones, 'match_laptops': match_laptops, 'match_printers': match_printers, 'match_barscanners': match_barscanners, 'match_cradles': match_cradles, 'match_scanners': match_scanners, 'match_bioscanners': match_bioscanners,'search_term': s})

def show_employee_info(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	return render(request, 'inventorysite/view_employee.html', {'employee': employee})

@login_required
def edit_employee_info(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	employee_form = EmployeeForm(instance = employee)
	if request.method == "POST" and employee.pk != 1:
		employee_form = EmployeeForm(request.POST, instance = employee)
		emp = employee_form.save()
		return redirect('inventorysite.views.show_employee_info', pk = emp.pk)
	else:
		employee_form = EmployeeForm(instance = employee)
	return render(request, 'inventorysite/edit_employee.html', {'form': employee_form, 'employee': employee})	
	
@login_required
#Set BooleanField to True and return all devices to OCME
def terminate_employee(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and employee.pk != 1:
		for computer in employee.computers.all():
			computer.pc_user = root_employee
			computer.save()
		for wdevice in employee.wdevices.all():
			wdevice.wd_date_returned = datetime.datetime.now().date()
			wdevice.wd_user = root_employee
			wdevice.save()
		for phone in employee.phones.all():
			phone.phone_user = root_employee
			phone.save()
		for laptop in employee.laptops.all():
			laptop.laptop_user = root_employee
			laptop.save()
		for printer in employee.printers.all():
			printer.printer_user = root_employee
			printer.save()
		for barscanner in employee.bscanners.all():
			barscanner.bs_user = root_employee
			barscanner.save()
		for cradle in employee.cradles.all():
			cradle.bsc_user = root_employee
			cradle.save()
		for scanner in employee.scanners.all():
			scanner.scanner_user = root_employee
			scanner.save()
		for bio in employee.bioscanners.all():
			bio.bis_user = root_employee
			bio.save()
		employee.is_terminated = True
		employee.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/terminate_employee_page.html', {'employee': employee})

@login_required
def add_pc(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = ComputerForm(request.POST)
		if form.is_valid():
			c = form.save(commit = False)
			c.pc_user = employee
			c.save()
			if employee.pk != 1:
				for root_computer in root_employee.computers.all():
					if c.pc_host_name == root_computer.pc_host_name and c.pc_service_tag == root_computer.pc_service_tag and c.pc_asset_tag == root_computer.pc_asset_tag:
						root_computer.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = ComputerForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee, 'add_pc': True})

@login_required
def delete_pc(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_pc = get_object_or_404(Computer, pk = pk2)
	if request.method == "POST":
		employee_pc.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_pc, 'employee': employee})
	
@login_required
def edit_pc(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_pc = get_object_or_404(Computer, pk = pk2)
	computer_form = ComputerForm(instance = employee_pc)
	if request.method == "POST":
		form = ComputerForm(request.POST, instance = employee_pc)
		if form.is_valid():
			c = form.save()
			if employee.pk != 1:
				for root_computer in root_employee.computers.all():
					if c.pc_host_name == root_computer.pc_host_name and c.pc_service_tag == root_computer.pc_service_tag and c.pc_asset_tag == root_computer.pc_asset_tag:
						root_computer.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		computer_form = ComputerForm(instance = employee_pc)
	return render(request, 'inventorysite/edit_device_page.html', {'form': computer_form, 'employee': employee, 'edit_pc': True})
	
@login_required
def return_pc(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_pc = get_object_or_404(Computer, pk = pk2)
	if request.method == "POST":
		employee_pc.pc_user = root_employee
		employee_pc.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_pc, 'employee': employee})	
		
@login_required
def add_wd(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = WirelessDeviceForm(request.POST)
		if form.is_valid():
			wd = form.save(commit = False)
			wd.wd_user = employee
			wd.save()
			if employee.pk != 1:
				for root_wd in root_employee.wdevices.all():
					if wd.wd_serial_number == root_wd.wd_serial_number and wd.wd_asset_tag == root_wd.wd_asset_tag:
						root_wd.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = WirelessDeviceForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee})
	
@login_required
def delete_wd(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_wd = get_object_or_404(WirelessDevice, pk = pk2)
	if request.method == "POST":
		employee_wd.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_wd, 'employee': employee})

@login_required
def edit_wd(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_wd = get_object_or_404(WirelessDevice, pk = pk2)
	wireless_device_form = WirelessDeviceForm(instance = employee_wd)
	if request.method == "POST":
		form = WirelessDeviceForm(request.POST, instance = employee_wd)
		if form.is_valid():
			wd = form.save()
			if employee.pk != 1:
				for root_wd in root_employee.wdevices.all():
					if wd.wd_serial_number == root_wd.wd_serial_number and wd.wd_asset_tag == root_wd.wd_asset_tag:
						root_wd.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		wireless_device_form = WirelessDeviceForm(instance = employee_wd)
	return render(request, 'inventorysite/edit_device_page.html', {'form': wireless_device_form, 'employee': employee})
	
@login_required
def return_wd(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_wd = get_object_or_404(WirelessDevice, pk = pk2)
	if request.method == "POST":
		employee_wd.wd_date_returned = datetime.datetime.now().date()
		employee_wd.wd_user = root_employee
		employee_wd.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_wd, 'employee': employee})

@login_required
def add_phone(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = IPPhoneForm(request.POST)
		if form.is_valid():
			p = form.save(commit = False)
			p.phone_user = employee
			p.save()
			if employee.pk != 1:
				for root_phone in root_employee.phones.all():
					if p.phone_serial_number == root_phone.phone_serial_number and p.phone_asset_tag == root_phone.phone_asset_tag:
						root_phone.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = IPPhoneForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee})
	
@login_required
def delete_phone(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_phone = get_object_or_404(IPPhone, pk = pk2)
	if request.method == "POST":
		employee_phone.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_phone, 'employee': employee})

@login_required
def edit_phone(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_phone = get_object_or_404(IPPhone, pk = pk2)
	phone_form = IPPhoneForm(instance = employee_phone)
	if request.method == "POST":
		form = IPPhoneForm(request.POST, instance = employee_phone)
		if form.is_valid():
			p = form.save()
			if employee.pk != 1:
				for root_phone in root_employee.phones.all():
					if p.phone_serial_number == root_phone.phone_serial_number and p.phone_asset_tag == root_phone.phone_asset_tag:
						root_phone.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		phone_form = IPPhoneForm(instance = employee_phone)
	return render(request, 'inventorysite/edit_device_page.html', {'form': phone_form, 'employee': employee})
	
@login_required
def return_phone(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_phone = get_object_or_404(IPPhone, pk = pk2)
	if request.method == "POST":
		employee_phone.phone_user = root_employee
		employee_phone.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_phone, 'employee': employee})
	
@login_required
def add_laptop(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = LaptopForm(request.POST)
		if form.is_valid():
			l = form.save(commit = False)
			l.laptop_user = employee
			l.save()
			if employee.pk != 1:
				for root_laptop in root_employee.laptops.all():
					if l.laptop_host_name == root_laptop.laptop_host_name and l.laptop_service_tag == root_laptop.laptop_service_tag and l.laptop_asset_tag == root_laptop.laptop_asset_tag:
						root_laptop.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = LaptopForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee})
	
@login_required
def delete_laptop(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_laptop = get_object_or_404(Laptop, pk = pk2)
	if request.method == "POST":
		employee_laptop.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_laptop, 'employee': employee})
	
@login_required
def edit_laptop(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_laptop = get_object_or_404(Laptop, pk = pk2)
	laptop_form = LaptopForm(instance = employee_laptop)
	if request.method == "POST":
		form = LaptopForm(request.POST, instance = employee_laptop)
		if form.is_valid():
			l = form.save()
			if employee.pk != 1:
				for root_laptop in root_employee.laptops.all():
					if l.laptop_host_name == root_laptop.laptop_host_name and l.laptop_service_tag == root_laptop.laptop_service_tag and l.laptop_asset_tag == root_laptop.laptop_asset_tag:
						root_laptop.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		laptop_form = LaptopForm(instance = employee_laptop)
	return render(request, 'inventorysite/edit_device_page.html', {'form': laptop_form, 'employee': employee})
	
@login_required
def return_laptop(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_laptop = get_object_or_404(Laptop, pk = pk2)
	if request.method == "POST":
		employee_laptop.laptop_user = root_employee
		employee_laptop.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_laptop, 'employee': employee})
	
@login_required
def add_printer(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = PrinterForm(request.POST)
		if form.is_valid():
			p = form.save(commit = False)
			p.printer_user = employee
			p.save()
			if employee.pk != 1:
				for root_printer in root_employee.printers.all():
					if p.printer_serial_number == root_printer.printer_serial_number and p.printer_asset_tag == root_printer.printer_asset_tag:
						root_printer.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = PrinterForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee})
	
@login_required
def delete_printer(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_printer = get_object_or_404(Printer, pk = pk2)
	if request.method == "POST":
		employee_printer.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_printer, 'employee': employee})
	
@login_required
def edit_printer(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_printer = get_object_or_404(Printer, pk = pk2)
	printer_form = LaptopForm(instance = employee_printer)
	if request.method == "POST":
		form = PrinterForm(request.POST, instance = employee_printer)
		if form.is_valid():
			p = form.save()
			if employee.pk != 1:
				for root_printer in root_employee.printers.all():
					if p.printer_serial_number == root_printer.printer_serial_number and p.printer_asset_tag == root_printer.printer_asset_tag:
						root_printer.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		printer_form = PrinterForm(instance = employee_printer)
	return render(request, 'inventorysite/edit_device_page.html', {'form': printer_form, 'employee': employee})
	
@login_required
def return_printer(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_printer = get_object_or_404(Printer, pk = pk2)
	if request.method == "POST":
		employee_printer.printer_user = root_employee
		employee_printer.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_printer, 'employee': employee})
	
@login_required
def add_bs(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = BarcodeScannerForm(request.POST)
		if form.is_valid():
			b = form.save(commit = False)
			b.bs_user = employee
			b.save()
			if employee.pk != 1:
				for root_barscanner in root_employee.bscanners.all():
					if b.bs_serial_number == root_barscanner.bs_serial_number and b.bs_asset_tag == root_barscanner.bs_asset_tag:
						root_barscanner.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = BarcodeScannerForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee})
	
@login_required
def delete_bs(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_bs = get_object_or_404(BarcodeScanner, pk = pk2)
	if request.method == "POST":
		employee_bs.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_bs, 'employee': employee})
	
@login_required
def edit_bs(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_bs = get_object_or_404(BarcodeScanner, pk = pk2)
	bs_form = BarcodeScannerForm(instance = employee_bs)
	if request.method == "POST":
		form = BarcodeScannerForm(request.POST, instance = employee_bs)
		if form.is_valid():
			b = form.save()
			if employee.pk != 1:
				for root_barscanner in root_employee.bscanners.all():
					if b.bs_serial_number == root_barscanner.bs_serial_number and b.bs_asset_tag == root_barscanner.bs_asset_tag:
						root_barscanner.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		bs_form = BarcodeScannerForm(instance = employee_bs)
	return render(request, 'inventorysite/edit_device_page.html', {'form': bs_form, 'employee': employee})
	
@login_required
def return_bs(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_bs = get_object_or_404(BarcodeScanner, pk = pk2)
	if request.method == "POST":
		employee_bs.bs_user = root_employee
		employee_bs.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_bs, 'employee': employee})
	
@login_required
def add_bsc(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = BarcodeScannerCradleForm(request.POST)
		if form.is_valid():
			b = form.save(commit = False)
			b.bsc_user = employee
			b.save()
			if employee.pk != 1:
				for root_cradle in root_employee.cradles.all():
					if b.bsc_serial_number == root_cradle.bsc_serial_number and b.bsc_asset_tag == root_cradle.bsc_asset_tag:
						root_cradle.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = BarcodeScannerCradleForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee})
	
@login_required
def delete_bsc(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_bsc = get_object_or_404(BarcodeScannerCradle, pk = pk2)
	if request.method == "POST":
		employee_bsc.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_bsc, 'employee': employee})
	
@login_required
def edit_bsc(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_bsc = get_object_or_404(BarcodeScannerCradle, pk = pk2)
	bsc_form = BarcodeScannerCradleForm(instance = employee_bsc)
	if request.method == "POST":
		form = BarcodeScannerCradleForm(request.POST, instance = employee_bsc)
		if form.is_valid():
			b = form.save()
			if employee.pk != 1:
				for root_cradle in root_employee.cradles.all():
					if b.bsc_serial_number == root_cradle.bsc_serial_number and b.bsc_asset_tag == root_cradle.bsc_asset_tag:
						root_cradle.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		bsc_form = BarcodeScannerCradleForm(instance = employee_bsc)
	return render(request, 'inventorysite/edit_device_page.html', {'form': bsc_form, 'employee': employee})

@login_required
def return_bsc(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_bsc = get_object_or_404(BarcodeScannerCradle, pk = pk2)
	if request.method == "POST":
		employee_bsc.bsc_user = root_employee
		employee_bsc.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_bsc, 'employee': employee})
	
@login_required
def add_scanner(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = ScannerForm(request.POST)
		if form.is_valid():
			s = form.save(commit = False)
			s.scanner_user = employee
			s.save()
			if employee.pk != 1:
				for root_scanner in root_employee.scanners.all():
					if s.scanner_serial_number == root_scanner.scanner_serial_number and s.scanner_asset_tag == root_scanner.scanner_asset_tag:
						root_scanner.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = ScannerForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee})
	
@login_required
def delete_scanner(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_scanner = get_object_or_404(Scanner, pk = pk2)
	if request.method == "POST":
		employee_scanner.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_scanner, 'employee': employee})
	
@login_required
def edit_scanner(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_scanner = get_object_or_404(Scanner, pk = pk2)
	scanner_form = ScannerForm(instance = employee_scanner)
	if request.method == "POST":
		form = ScannerForm(request.POST, instance = employee_scanner)
		if form.is_valid():
			s = form.save()
			if employee.pk != 1:
				for root_scanner in root_employee.scanners.all():
					if s.scanner_serial_number == root_scanner.scanner_serial_number and s.scanner_asset_tag == root_scanner.scanner_asset_tag:
						root_scanner.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		scanner_form = ScannerForm(instance = employee_scanner)
	return render(request, 'inventorysite/edit_device_page.html', {'form': scanner_form, 'employee': employee})
	
@login_required
def return_scanner(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_scanner = get_object_or_404(Scanner, pk = pk2)
	if request.method == "POST":
		employee_scanner.scanner_user = root_employee
		employee_scanner.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_scanner, 'employee': employee})
	
@login_required
def add_bio(request, pk):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	if request.method == "POST" and not employee.is_terminated:
		form = BiometricScannerForm(request.POST)
		if form.is_valid():
			b = form.save(commit = False)
			b.bis_user = employee
			b.save()
			if employee.pk != 1:
				for root_bioscanner in root_employee.bioscanners.all():
					if b.bis_serial_number == root_bioscanner.bis_serial_number and b.bis_asset_tag == root_bioscanner.bis_asset_tag:
						root_bioscanner.delete()
			return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		form = BiometricScannerForm()
	return render(request, 'inventorysite/add_device_page.html', {'form': form, 'employee': employee})
	
@login_required
def delete_bio(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	employee_bioscanner = get_object_or_404(BiometricScanner, pk = pk2)
	if request.method == "POST":
		employee_bioscanner.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/delete_device_page.html', {'employee_device': employee_bioscanner, 'employee': employee})

@login_required
def edit_bio(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_bioscanner = get_object_or_404(BiometricScanner, pk = pk2)
	bioscanner_form = BiometricScannerForm(instance = employee_bioscanner)
	if request.method == "POST":
		form = BiometricScannerForm(request.POST, instance = employee_bioscanner)
		if form.is_valid():
			b = form.save()
			if employee.pk != 1:
				for root_bioscanner in root_employee.bioscanners.all():
					if b.bis_serial_number == root_bioscanner.bis_serial_number and b.bis_asset_tag == root_bioscanner.bis_asset_tag:
						root_bioscanner.delete()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	else:
		bioscanner_form = BiometricScannerForm(instance = employee_bioscanner)
	return render(request, 'inventorysite/edit_device_page.html', {'form': bioscanner_form, 'employee': employee})
	
@login_required
def return_bio(request, pk, pk2):
	employee = get_object_or_404(Employee, pk = pk)
	root_employee = get_object_or_404(Employee, pk = 1)
	employee_bioscanner = get_object_or_404(BiometricScanner, pk = pk2)
	if request.method == "POST":
		employee_bioscanner.bis_user = root_employee
		employee_bioscanner.save()
		return redirect('inventorysite.views.show_employee_info', pk = employee.pk)
	return render(request, 'inventorysite/return_device_page.html', {'employee_device': employee_bioscanner, 'employee': employee})

@login_required
def show_area_detail(request, location, department):
	if location == "all" and department == "none":
		all_locations = []
		for employee in Employee.objects.all():
			if employee.location not in all_locations and employee.location != "":
				all_locations.append(employee.location)
		return render(request, 'inventorysite/show_area_page.html', {'location': location, 'department': department, 'all_locations': all_locations})
	elif location == "none" and department == "all":
		dna_departments = []
		ft_departments = []
		bronx_departments = []
		queens_departments = []
		si_departments = []
		for employee in Employee.objects.all():
			if employee.location == "DNA" and employee.department not in dna_departments and employee.department != "OCME":
				dna_departments.append(employee.department)
			elif employee.location == "520" and employee.department not in ft_departments:
				ft_departments.append(employee.department)
			elif employee.location == "Bronx" and employee.department not in bronx_departments:
				bronx_departments.append(employee.department)
			elif employee.location == "Queens" and employee.department not in queens_departments:
				queens_departments.append(employee.department)
			elif employee.location == "SI" and employee.department not in si_departments:
				si_departments.append(employee.department)
		return render(request, 'inventorysite/show_area_page.html', {'location': location, 'department': department, 'dna_departments': dna_departments, 'ft_departments': ft_departments, 'bronx_departments': bronx_departments, 'queens_departments': queens_departments, 'si_departments': si_departments})
	elif department == "all":
		all_departments = []
		for employee in Employee.objects.all():
			if employee.location == location and employee.department not in all_departments and employee.department != "OCME":
				all_departments.append(employee.department)
		return render(request, 'inventorysite/show_area_page.html', {'location': location, 'department': department, 'all_departments': all_departments})
	else:
		match_employees = []
		for employee in Employee.objects.all():
			if employee.location == location and employee.department == department:
				match_employees.append(employee)
		return render(request, 'inventorysite/show_area_page.html', {'location': location, 'department': department, 'match_employees': match_employees})

@login_required
def show_all_employees(request):
	all_employees = Employee.objects.all()
	return render(request, 'inventorysite/all_employees_page.html', {'all_employees': all_employees})

@login_required
def show_all_computers(request):
	all_computers = Computer.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_computers': all_computers, 'show_computers': True})

@login_required
def show_all_wdevices(request):
	all_wdevices = WirelessDevice.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_wdevices': all_wdevices, 'show_wdevices': True})
	
@login_required
def show_all_phones(request):
	all_phones = IPPhone.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_phones': all_phones, 'show_phones': True})
	
@login_required
def show_all_laptops(request):
	all_laptops = Laptop.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_laptops': all_laptops, 'show_laptops': True})
	
@login_required
def show_all_printers(request):
	all_printers = Printer.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_printers': all_printers, 'show_printers': True})
	
@login_required
def show_all_barscanners(request):
	all_barscanners = BarcodeScanner.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_barscanners': all_barscanners, 'show_barscanners': True})
	
@login_required
def show_all_cradles(request):
	all_cradles = BarcodeScannerCradle.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_cradles': all_cradles, 'show_cradles': True})
	
@login_required
def show_all_scanners(request):
	all_scanners = Scanner.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_scanners': all_scanners, 'show_scanners': True})
	
@login_required
def show_all_bioscanners(request):
	all_bioscanners = BiometricScanner.objects.all()
	return render(request, 'inventorysite/all_device_page.html', {'all_bioscanners': all_bioscanners, 'show_bioscanners': True})

var numClicks = 0; //Odd/even counter for showing ascend or descend for table sorting

$(document).ready(function() {

	$("#pcForm").remove();
	$("#id_computers-TOTAL_FORMS").val(0);        //Visually hide forms and set num_forms to zero
	$("#wdForm").remove();
	$("#id_wdevices-TOTAL_FORMS").val(0);
	$("#phoneForm").remove();
	$("#id_phones-TOTAL_FORMS").val(0);
	$("#laptopForm").remove();
	$("#id_laptops-TOTAL_FORMS").val(0);
	$("#printerForm").remove();
	$("#id_printers-TOTAL_FORMS").val(0);
	$("#barcodeForm").remove();
	$("#id_barscanners-TOTAL_FORMS").val(0);
	$("#cradleForm").remove();
	$("#id_cradles-TOTAL_FORMS").val(0);
	$("#scannerForm").remove();
	$("#id_scanners-TOTAL_FORMS").val(0);
	$("#bioForm").remove();
	$("#id_bioscanners-TOTAL_FORMS").val(0);
	

	$(document).tooltip();      //Show when user hovers over td element
	
	$(".datepicker").datepicker({ 
		changeYear: true,
		changeMonth: true,
	});
	
	//Add form for requested device, increment number of forms, move buttons, scroll page down
	$("#add_pc").click(function() {
		var numPC = $("#id_computers-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newPCForm").html().replace(/__prefix__/g, numPC));
		$("#id_computers-TOTAL_FORMS").val(parseInt(numPC) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	$("#add_wd").click(function() {
		var numWD = $("#id_wdevices-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newWDForm").html().replace(/__prefix__/g, numWD));
		$("#id_wdevices-TOTAL_FORMS").val(parseInt(numWD) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	$("#add_phone").click(function() {
		var numPhones = $("#id_phones-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newPhoneForm").html().replace(/__prefix__/g, numPhones));
		$("#id_phones-TOTAL_FORMS").val(parseInt(numPhones) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	$("#add_laptop").click(function() {
		var numLaptops = $("#id_laptops-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newLaptopForm").html().replace(/__prefix__/g, numLaptops));
		$("#id_laptops-TOTAL_FORMS").val(parseInt(numLaptops) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	$("#add_printer").click(function() {
		var numPrinters = $("#id_printers-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newPrinterForm").html().replace(/__prefix__/g, numPrinters));
		$("#id_printers-TOTAL_FORMS").val(parseInt(numPrinters) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	$("#add_bs").click(function() {
		var numBarcodeScanners = $("#id_barscanners-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newBarcodeForm").html().replace(/__prefix__/g, numBarcodeScanners));
		$("#id_barscanners-TOTAL_FORMS").val(parseInt(numBarcodeScanners) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	$("#add_bsc").click(function() {
		var numCradles = $("#id_cradles-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newCradleForm").html().replace(/__prefix__/g, numCradles));
		$("#id_cradles-TOTAL_FORMS").val(parseInt(numCradles) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	$("#add_scanner").click(function() {
		var numScanners = $("#id_scanners-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newScannerForm").html().replace(/__prefix__/g, numScanners));
		$("#id_scanners-TOTAL_FORMS").val(parseInt(numScanners) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	$("#add_bis").click(function() {
		var numBioScanners = $("#id_bioscanners-TOTAL_FORMS").val();
		$("#addEmployeeForm").append($(".newBioForm").html().replace(/__prefix__/g, numBioScanners));
		$("#id_bioscanners-TOTAL_FORMS").val(parseInt(numBioScanners) + 1);
		$("#submitEmployeeInfo").appendTo($("#addEmployeeForm"));
		$("#addDeviceButtons").appendTo($("#addEmployeeForm"));
		var y = $(window).scrollTop();
		$(window).scrollTop(y + 600);
	});
	
	//Sort table columns
	$('th').click(function () {
		numClicks += 1;
		if (numClicks % 2 == 0) {
			$("#descend").remove();
			$(this).append("<span style='float: right' id='ascend' class='glyphicon glyphicon-chevron-up'></span>");
		} else {
			$("#ascend").remove();
			$(this).append("<span style='float: right' id='descend' class='glyphicon glyphicon-chevron-down'></span>");
		}
    	var table = $(this).parents('table').eq(0);
    	var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));
    	this.asc = !this.asc;
    	if (!this.asc){rows = rows.reverse();}
    	for (var i = 0; i < rows.length; i++){table.append(rows[i]);}
	});
	function comparer(index) {
    	return function(a, b) {
        	var valA = getCellValue(a, index), valB = getCellValue(b, index);
        	return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB);
    	}
	}
	function getCellValue(row, index){ return $(row).children('td').eq(index).html(); }
});

/*Show form that will be removed when user hovers over delete button*/
$(document).on("mouseenter", ".delete_device", function() {
	$(this).closest(".removableDiv").css("background-color", "#BDDEFF");
});

$(document).on("mouseleave", ".delete_device", function() {
	$(this).closest(".removableDiv").css("background-color", "initial");
});

$(document).on("click", ".delete_device", function() {     //Delete containers of these forms
	$(this).closest(".removableDiv").remove();
});

$(document).on("click", "#add_wd", function() {
	$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
	});
});

function validateForm() {     //Reject form if either last or first name is missing
	var lastName = document.forms["employeeInfoForm"]["last_name"].value;
	var firstName = document.forms["employeeInfoForm"]["first_name"].value;
	var error1 = "";
	var error2 = "";
	if(lastName == null || lastName == "") {
		error1 = "Last name is required";
		errors = true;
	}
	if(firstName == null || firstName == "") {
		error2 = "First name is required";
		errors = true;
	}
	if(error1 != "" || error2 != "") {
		alert(error1 + "\n" + error2);
		return false;
	}
}
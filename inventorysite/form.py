from django import forms
from .models import Search, Employee, Computer, WirelessDevice, IPPhone, Laptop, Printer, BarcodeScanner, BarcodeScannerCradle, Scanner, BiometricScanner
from django.contrib.auth.models import User
from django.contrib.admin.widgets import AdminDateWidget
from functools import partial

class EmployeeForm(forms.ModelForm):
	class Meta:
		model = Employee
		fields = ['last_name', 'first_name', 'location', 'department', 'employee_id', 'email',]
		labels = {'last_name': 'Last Name (required)', 'first_name': 'First Name (required)', 'employee_id': 'Employee ID', 'email': 'E-mail'}

class LoginForm(forms.ModelForm):
	password = forms.CharField(widget = forms.PasswordInput())
	#Hide help text
	def __init__(self, *args, **kwargs):
		super(LoginForm, self).__init__(*args, **kwargs)
		for field in ['username', 'password']:
			self.fields[field].help_text = None
	class Meta:
		model = User
		fields = ['username', 'password',]
		
class SearchForm(forms.ModelForm):
	class Meta:
		model = Search
		fields = ["search_term",]
		labels = {'search_term': 'Enter any string to be searched',}
		
class ComputerForm(forms.ModelForm):
	class Meta:
		model = Computer
		fields = ['pc_manufacturer', 'pc_model', 'pc_host_name', 'pc_service_tag', 'pc_asset_tag', 'pc_owner',]
		labels = {'pc_manufacturer': 'PC Manufacturer', 'pc_model': 'PC Model', 'pc_host_name': 'PC Host Name', 'pc_service_tag': 'PC Serial Num/Service Tag', 'pc_asset_tag': 'PC Asset Tag', 'pc_owner': 'PC Owner',}

class WirelessDeviceForm(forms.ModelForm):
	class Meta:
		model = WirelessDevice
		fields = ['wd_type', 'wd_manufacturer', 'wd_model', 'wd_number', 'wd_serial_number', 'wd_meid', 'wd_carrier', 'wd_asset_tag', 'wd_date_deployed', 'wd_date_returned', ]
		labels = {'wd_type': 'WD Type', 'wd_manufacturer': 'WD Manufacturer', 'wd_model': 'WD Model', 'wd_number': 'WD Number', 'wd_serial_number': 'WD Serial Number', 'wd_meid': 'MEID', 'wd_carrier': 'WD Carrier', 'wd_asset_tag': 'WD Asset Tag', 'wd_date_deployed': 'WD Deploy Date', 'wd_date_returned': 'WD Return Date',}	
		#Add widget for jQuery datepicker
		widgets = {
			'wd_date_deployed': forms.DateInput(attrs = {'class': 'datepicker'}),
            'wd_date_returned': forms.DateInput(attrs = {'class': 'datepicker'}),
		}

class IPPhoneForm(forms.ModelForm):
	class Meta:
		model = IPPhone
		fields = ['phone_manufacturer', 'phone_model','phone_number', 'phone_serial_number', 'phone_mac', 'phone_asset_tag', ]
		labels = {'phone_manufacturer': 'Phone Manufacturer', 'phone_model': 'Phone Model','phone_number': 'Phone Number', 'phone_serial_number': 'Phone Serial Number', 'phone_mac': 'Phone MAC', 'phone_asset_tag': 'Phone Asset Tag',}
		
class LaptopForm(forms.ModelForm):
	class Meta:
		model = Laptop
		fields = ['laptop_manufacturer', 'laptop_model', 'laptop_host_name', 'laptop_service_tag', 'laptop_asset_tag', 'laptop_owner', ]
		labels = {'laptop_manufacturer': 'Laptop Manufacturer', 'laptop_model': 'Laptop Model', 'laptop_host_name': 'Laptop Host Name', 'laptop_service_tag': 'Laptop Service Tag', 'laptop_asset_tag': 'Laptop Asset Tag', 'laptop_owner': 'Laptop Owner', }

class PrinterForm(forms.ModelForm):
	class Meta:
		model = Printer
		fields = ['printer_manufacturer', 'printer_model', 'printer_serial_number', 'printer_asset_tag', 'printer_owner', ]
		labels = {'printer_manufacturer': 'Printer Manufacturer', 'printer_model': 'Printer Model', 'printer_serial_number': 'Printer Serial Number', 'printer_asset_tag': 'Printer Asset Tag', 'printer_owner': 'Printer Owner',}

class BarcodeScannerForm(forms.ModelForm):
	class Meta:
		model = BarcodeScanner
		fields = ['bs_manufacturer', 'bs_model', 'bs_serial_number', 'bs_asset_tag', ]
		labels = {'bs_manufacturer': 'BS Manufacturer', 'bs_model': 'BS Model', 'bs_serial_number': 'BS Serial Number', 'bs_asset_tag': 'BS Asset Tag', }
	
class BarcodeScannerCradleForm(forms.ModelForm):
	class Meta:
		model = BarcodeScannerCradle
		fields = ['bsc_manufacturer', 'bsc_model', 'bsc_serial_number', 'bsc_asset_tag', ]
		labels = {'bsc_manufacturer': 'Cradle Manufacturer', 'bsc_model': 'Cradle Model', 'bsc_serial_number': 'Cradle Serial Number', 'bsc_asset_tag': 'Cradle Asset Tag', }

class ScannerForm(forms.ModelForm):
	class Meta:
		model = Scanner
		fields = ['scanner_manufacturer', 'scanner_model', 'scanner_serial_number', 'scanner_asset_tag', ]
		labels = {'scanner_manufacturer': 'Scanner Manufacturer', 'scanner_model': 'Scanner Model', 'scanner_serial_number': 'Scanner Serial Number', 'scanner_asset_tag': 'Scanner Asset Tag', }
		
class BiometricScannerForm(forms.ModelForm):
	class Meta:
		model = BiometricScanner
		fields = ['bis_manufacturer', 'bis_model', 'bis_serial_number', 'bis_asset_tag', ]
		labels = {'bis_manufacturer': 'Biometric Scanner Manufacturer', 'bis_model': 'Biometric Scanner Model', 'bis_serial_number': 'Biometric Scanner Serial Number', 'bis_asset_tag': 'Biometric Scanner Asset Tag', }
		
		

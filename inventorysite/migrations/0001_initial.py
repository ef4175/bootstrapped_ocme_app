# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BarcodeScanner',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('bs_manufacturer', models.CharField(max_length=64, blank=True)),
                ('bs_model', models.CharField(max_length=64, blank=True)),
                ('bs_serial_number', models.CharField(max_length=64, blank=True)),
                ('bs_asset_tag', models.CharField(max_length=64, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='BarcodeScannerCradle',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('bsc_manufacturer', models.CharField(max_length=64, blank=True)),
                ('bsc_model', models.CharField(max_length=64, blank=True)),
                ('bsc_serial_number', models.CharField(max_length=64, blank=True)),
                ('bsc_asset_tag', models.CharField(max_length=64, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='BiometricScanner',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('bis_manufacturer', models.CharField(max_length=64, blank=True)),
                ('bis_model', models.CharField(max_length=64, blank=True)),
                ('bis_serial_number', models.CharField(max_length=64, blank=True)),
                ('bis_asset_tag', models.CharField(max_length=64, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Computer',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('pc_manufacturer', models.CharField(choices=[('Dell', 'Dell'), ('Lenovo', 'Lenovo'), ('HP', 'HP'), ('Asus', 'Asus'), ('Microsoft', 'Microsoft')], max_length=32, blank=True)),
                ('pc_model', models.CharField(choices=[('9020', '9020'), ('9010', '9010'), ('780', '780'), ('790', '790'), ('CF-30', 'CF-30'), ('CF-19', 'CF-19'), ('Surface Pro 3', 'Surface Pro 3')], max_length=32, blank=True)),
                ('pc_host_name', models.CharField(max_length=64, blank=True)),
                ('pc_service_tag', models.CharField(max_length=64, blank=True)),
                ('pc_asset_tag', models.CharField(max_length=64, blank=True)),
                ('pc_owner', models.CharField(choices=[('OCME', 'OCME'), ('SPO', 'SPO'), ('Grant', 'Grant')], max_length=64, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('last_name', models.CharField(max_length=64)),
                ('first_name', models.CharField(max_length=64)),
                ('location', models.CharField(choices=[('DNA', 'DNA'), ('520', '520'), ('Bronx', 'Bronx'), ('Queens', 'Queens'), ('SI', 'SI')], max_length=32, blank=True)),
                ('department', models.CharField(max_length=64, blank=True)),
                ('employee_id', models.CharField(max_length=64, blank=True)),
                ('is_terminated', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='IPPhone',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('phone_manufacturer', models.CharField(choices=[('Cisco', 'Cisco')], max_length=32, blank=True)),
                ('phone_model', models.CharField(max_length=64, blank=True)),
                ('phone_number', models.CharField(max_length=64, blank=True)),
                ('phone_serial_number', models.CharField(max_length=64, blank=True)),
                ('phone_mac', models.CharField(max_length=64, blank=True)),
                ('phone_asset_tag', models.CharField(max_length=64, blank=True)),
                ('phone_user', models.ForeignKey(related_name='phones', to='inventorysite.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='Laptop',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('laptop_manufacturer', models.CharField(max_length=64, blank=True)),
                ('laptop_model', models.CharField(max_length=64, blank=True)),
                ('laptop_host_name', models.CharField(max_length=64, blank=True)),
                ('laptop_service_tag', models.CharField(max_length=64, blank=True)),
                ('laptop_asset_tag', models.CharField(max_length=64, blank=True)),
                ('laptop_owner', models.CharField(choices=[('OCME', 'OCME'), ('SPO', 'SPO'), ('Grant', 'Grant')], max_length=32, blank=True)),
                ('laptop_user', models.ForeignKey(related_name='laptops', to='inventorysite.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='Printer',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('printer_manufacturer', models.CharField(choices=[('Xerox', 'Xerox'), ('HP', 'HP'), ('Zebra', 'Zebra')], max_length=32, blank=True)),
                ('printer_model', models.CharField(max_length=64, blank=True)),
                ('printer_serial_number', models.CharField(max_length=64, blank=True)),
                ('printer_asset_tag', models.CharField(max_length=64, blank=True)),
                ('printer_owner', models.CharField(choices=[('OCME', 'OCME'), ('SPO', 'SPO'), ('Grant', 'Grant')], max_length=32, blank=True)),
                ('printer_user', models.ForeignKey(related_name='printers', to='inventorysite.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='Scanner',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('scanner_manufacturer', models.CharField(choices=[('Epson', 'Epson'), ('Fujitsu', 'Fujitsu')], max_length=32, blank=True)),
                ('scanner_model', models.CharField(choices=[('DS-510', 'DS-510'), ('DS-520', 'DS-520'), ('V-700', 'V-700'), ('GT-1500', 'GT-1500'), ('ix500', 'ix500'), ('s500', 's500'), ('s510', 's510'), ('s1500', 's1500')], max_length=32, blank=True)),
                ('scanner_serial_number', models.CharField(max_length=64, blank=True)),
                ('scanner_asset_tag', models.CharField(max_length=64, blank=True)),
                ('scanner_user', models.ForeignKey(related_name='scanners', to='inventorysite.Employee')),
            ],
        ),
        migrations.CreateModel(
            name='Search',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('search_term', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='WirelessDevice',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('wd_type', models.CharField(choices=[('Smart Phone', 'Smart Phone'), ('MiFi', 'MiFi'), ('Cell Phone', 'Cell Phone')], max_length=32, blank=True)),
                ('wd_manufacturer', models.CharField(choices=[('Samsung', 'Samsung'), ('Blackberry', 'Blackberry')], max_length=32, blank=True)),
                ('wd_carrier', models.CharField(choices=[('Verizon', 'Verizon'), ('AT&T', 'AT&T'), ('Sprint', 'Sprint')], max_length=32, blank=True)),
                ('wd_model', models.CharField(choices=[('Blackberry', 'Blackberry'), ('Galaxy S5', 'Galaxy S5'), ('MiFi', 'MiFi')], max_length=32, blank=True)),
                ('wd_number', models.CharField(max_length=64, blank=True)),
                ('wd_serial_number', models.CharField(max_length=64, blank=True)),
                ('wd_meid', models.CharField(max_length=64, blank=True)),
                ('wd_asset_tag', models.CharField(max_length=64, blank=True)),
                ('wd_date_deployed', models.DateField(null=True, blank=True)),
                ('wd_date_returned', models.DateField(null=True, blank=True)),
                ('wd_user', models.ForeignKey(related_name='wdevices', to='inventorysite.Employee')),
            ],
        ),
        migrations.AddField(
            model_name='computer',
            name='pc_user',
            field=models.ForeignKey(related_name='computers', to='inventorysite.Employee'),
        ),
        migrations.AddField(
            model_name='biometricscanner',
            name='bis_user',
            field=models.ForeignKey(related_name='bioscanners', to='inventorysite.Employee'),
        ),
        migrations.AddField(
            model_name='barcodescannercradle',
            name='bsc_user',
            field=models.ForeignKey(related_name='cradles', to='inventorysite.Employee'),
        ),
        migrations.AddField(
            model_name='barcodescanner',
            name='bs_user',
            field=models.ForeignKey(related_name='bscanners', to='inventorysite.Employee'),
        ),
    ]
